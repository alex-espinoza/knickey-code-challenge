window.slate = window.slate || {};
window.theme = window.theme || {};

/*================ Slate ================*/
// =require slate/a11y.js
// =require slate/cart.js
// =require slate/utils.js
// =require slate/rte.js
// =require slate/sections.js
// =require slate/currency.js
// =require slate/images.js
// =require slate/variants.js

/*================ Sections ================*/
// =require sections/product.js

/*================ Templates ================*/
// =require templates/customers-addresses.js
// =require templates/customers-login.js

$(document).ready(function() {
  var sections = new slate.Sections();
  sections.register('product', theme.Product);

  // Common a11y fixes
  slate.a11y.pageLinkFocus($(window.location.hash));

  $('.in-page-link').on('click', function(evt) {
    slate.a11y.pageLinkFocus($(evt.currentTarget.hash));
  });

  // Target tables to make them scrollable
  var tableSelectors = '.rte table';

  slate.rte.wrapTable({
    $tables: $(tableSelectors),
    tableWrapperClass: 'rte__table-wrapper',
  });

  // Target iframes to make them responsive
  var iframeSelectors =
    '.rte iframe[src*="youtube.com/embed"],' +
    '.rte iframe[src*="player.vimeo"]';

  slate.rte.wrapIframe({
    $iframes: $(iframeSelectors),
    iframeWrapperClass: 'rte__video-wrapper'
  });

  // Apply a specific class to the html element for browser support of cookies.
  if (slate.cart.cookiesEnabled()) {
    document.documentElement.className = document.documentElement.className.replace('supports-no-cookies', 'supports-cookies');
  }

  // Handle getting height of product drop-down nav menu
  var $productDropDownMenu = $('.product-drop-down-menu');
  var $clonedProductDropDownMenu = $productDropDownMenu.clone().insertAfter('body');
  var clonedProductDropDownMenuCSS = $clonedProductDropDownMenu.attr('style');
  var productDropDownMenuHeight = 0;

  function getProductDropDownMenuHeight() {
    $clonedProductDropDownMenu.css({
      position: 'fixed',
      visibility: 'hidden',
      height: 'auto'
    });

    productDropDownMenuHeight = $clonedProductDropDownMenu.height();
    $clonedProductDropDownMenu.attr('style', clonedProductDropDownMenuCSS);
  }

  getProductDropDownMenuHeight();

  $(window).resize(function() {
    getProductDropDownMenuHeight();
  });

  // Handle opening product drop-down nav menu
  var $navbarToggler = $('.navbar-toggler');
  var $productDropDownMenuButton = $('.product-drop-down-menu-button');
  var $headerSection = $('#shopify-section-header');
  var $navShadow = $('.nav-shadow');

  function handleShowProductDropDownMenu() {
    var productDropDownMenuAlreadyOpen = $productDropDownMenu.hasClass('open');

    if (productDropDownMenuAlreadyOpen) {
      handleHideProductDropDownMenu();
    } else {
      $productDropDownMenu.addClass('open');
      $headerSection.addClass('product-drop-down-open');
      $navShadow.addClass('active');
      $productDropDownMenu.height(productDropDownMenuHeight);
    }
  }

  function handleHideProductDropDownMenu() {
    $productDropDownMenu.removeClass('open');
    $headerSection.removeClass('product-drop-down-open');
    $navShadow.removeClass('active');
    $productDropDownMenu.height(0);
  }

  $productDropDownMenuButton.on('click', function() {
    handleShowProductDropDownMenu();
  });

  $navShadow.on('click', function() {
    handleHideProductDropDownMenu();
  });

  $navbarToggler.on('click', function() {
    handleHideProductDropDownMenu();
  });
});
